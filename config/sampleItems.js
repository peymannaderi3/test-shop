export default [
  {
    id: 1,
    title: "مانیتور سامسونگ مدل C24F390 سایز 24 اینچ",
    amazing: true,
    colors: [
      {
        id: 1,
        name: "قرمز",
        code: "red"
      },
      {
        id: 2,
        name: "آبی",
        code: "blue"
      }
    ],
    desc: "توضیحات محصول شماره ۱",
    image:
      "https://dkstatics-public.digikala.com/digikala-products/1223511.jpg?x-oss-process=image/resize,h_1600/quality,q_80",
    price: {
      discount_percent: 10,
      amount: 1000,
      final_amount: 900
    }
  },
  {
    id: 2,
    title: "مانیتور مسترتک مدل PA245Q سایز 24 اینچ",
    amazing: true,
    colors: [
      {
        id: 3,
        name: "مشکی",
        code: "black"
      }
    ],
    desc: "توضیحات محصول شماره 2",
    image:
      "https://dkstatics-public.digikala.com/digikala-products/112555954.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
    price: {
      discount_percent: 10,
      amount: 3600000,
      final_amount: 3500000
    }
  },
  {
    id: 3,
    title: "مانیتور ایسوس مدل VS197DE سایز 18.5 اینچ",
    amazing: false,
    colors: [
      {
        id: 4,
        name: "سبز",
        code: "green"
      },
      {
        id: 2,
        name: "آبی",
        code: "blue"
      }
    ],
    desc: "توضیحات محصول شماره 3",
    image:
      "https://dkstatics-public.digikala.com/digikala-products/138183.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
    price: {
      discount_percent: 0,
      amount: 2000,
      final_amount: 2000
    }
  }
];
