import keys from "@/config/keys";

export const actions = {
  nuxtClientInit({ commit }) {
    const cartItems = JSON.parse(localStorage.getItem(keys.cart)) || [];
    commit("cart/SET_ITEMS", cartItems);
  }
};
