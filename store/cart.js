import keys from "@/config/keys";

export const state = () => ({
  items: []
});

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },
  CLEAN_CART(state) {
    state.items = [];

    localStorage.setItem(keys.cart, JSON.stringify(state.items));
  },
  INCREASE_AMOUNT(state, payload) {
    const { amount, price, product } = payload;
    const { items } = state;

    const indexProduct = items.findIndex(
      item => item.product.id === product.id
    );

    if (indexProduct > -1) {
      items[indexProduct].amount += 1;
    } else {
      items.push({ amount, price, product });
    }
    state.items = items;

    localStorage.setItem(keys.cart, JSON.stringify(items));
  },
  DECREASE_AMOUNT(state, payload) {
    const { amount, price, product } = payload;
    let { items } = state;

    const indexProduct = items.findIndex(
      item => item.product.id === product.id
    );

    if (indexProduct > -1) {
      let newAmount = items[indexProduct].amount - 1;
      if (newAmount > 0) items[indexProduct].amount = newAmount;
      else {
        items.splice(indexProduct, 1);
      }
      state.items = items;

      localStorage.setItem(keys.cart, JSON.stringify(items));
    }
  }
};

export const actions = {
  increaseAmount({ commit }, payload) {
    commit("INCREASE_AMOUNT", payload);
  },
  decreaseAmount({ commit }, payload) {
    commit("DECREASE_AMOUNT", payload);
  },
  clearCart({ commit }) {
    commit("CLEAN_CART");
  }
};

export const getters = {};
