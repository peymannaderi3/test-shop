import Vue from "vue";

const global = {
  methods: {
    numberWithCommas(x) {
      if (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      } else {
        return "";
      }
    }
  }
};

Vue.mixin(global);
